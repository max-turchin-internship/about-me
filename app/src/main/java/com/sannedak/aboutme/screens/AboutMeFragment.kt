package com.sannedak.aboutme.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollableColumn
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.ResourceFont
import androidx.compose.ui.text.font.fontFamily
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.ui.tooling.preview.Preview
import com.sannedak.aboutme.R
import com.sannedak.aboutme.ui.AboutMeTheme
import com.sannedak.aboutme.ui.nameStyle

class AboutMeFragment : Fragment() {

    @ExperimentalAnimationApi
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            AboutMeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    AboutMe()
                }
            }
        }
    }

    @ExperimentalAnimationApi
    @Composable
    private fun AboutMe() {
        val textState = remember { mutableStateOf("") }
        val inputState = remember { mutableStateOf(true) }

        Column {
            TopAppBar(title = { Text(text = stringResource(id = R.string.app_name)) })
            Column(
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .fillMaxSize()
            ) {
                val nameModifier = Modifier
                    .padding(top = 16.dp)
                    .align(Alignment.CenterHorizontally)
                    .padding(top = 8.dp)
                val imageModifier = Modifier
                    .padding(top = 16.dp)
                    .align(Alignment.CenterHorizontally)
                val inputModifier = Modifier
                    .padding(top = 16.dp)
                    .fillMaxWidth()
                val buttonModifier = Modifier
                    .padding(top = 16.dp)
                    .align(Alignment.CenterHorizontally)

                NameText(nameModifier)
                AnimatedVisibility(visible = inputState.value) {
                    Column {
                        OutlinedTextField(
                            value = textState.value,
                            onValueChange = { textState.value = it },
                            modifier = inputModifier,
                            textStyle = nameStyle,
                            label = { Text(text = stringResource(id = R.string.what_is_your_nickname)) },
                            maxLines = 1
                        )
                        Button(onClick = { inputState.value = false }, modifier = buttonModifier) {
                            Text(
                                text = stringResource(id = R.string.done),
                                style = TextStyle(fontFamily = fontFamily(ResourceFont(resId = R.font.roboto)))
                            )
                        }
                    }
                }
                AnimatedVisibility(visible = !inputState.value) {
                    Column(modifier = Modifier.fillMaxWidth()) {
                        Text(text = textState.value, modifier = nameModifier, style = nameStyle)
                    }
                }
                Image(asset = Icons.Default.Star, modifier = imageModifier)
                ScrollableColumn(modifier = Modifier.padding(top = 16.dp)) {
                    Text(text = stringResource(id = R.string.bio), style = nameStyle)
                }
            }
        }
    }

    @Composable
    private fun NameText(nameModifier: Modifier) {
        Text(
            text = stringResource(id = R.string.name),
            modifier = nameModifier,
            style = nameStyle
        )
    }

    @ExperimentalAnimationApi
    @Preview(showBackground = true)
    @Composable
    private fun DefaultPreview() {
        AboutMeTheme {
            AboutMe()
        }
    }
}